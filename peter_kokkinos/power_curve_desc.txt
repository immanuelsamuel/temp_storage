Power Analysis:

Assuming 50% prevalence of LVH in general population we hypothesize that there are significant differences in blood pressure between those with LVH and those without LVH. Power analysis for a two-tailed t-test at 0.05 significance was done for small (0.2), medium (0.5) and large (0.8) effect sizes (Cohen's-d) as show in the figure. A sample size of 93 would have sufficient power to test this difference with a medium effect size at p=0.05.
